import React from "react";
import { Container, Typography, Grid, Button } from "@material-ui/core";
import { classes } from "istanbul-lib-coverage";

const Cart = () => {
    const isEmpty = true;

    const EmptyCart = () => {
        <Typography>
            {" "}
            You have no items in your shopping cart. please add some!{" "}
        </Typography>;
    };
    const FilledCart = () => {
        <>
            <Grid container spacing={3}></Grid>
        </>;
    };

    return (
        <Container>
            <div className={classes.toolbar}></div>
            <Typography className={classes.title} variant="h3">
                Your shopping cart
            </Typography>
            {isEmpty ? <EmptyCart /> : <FilledCart />}
        </Container>
    );
};

export default Cart;
