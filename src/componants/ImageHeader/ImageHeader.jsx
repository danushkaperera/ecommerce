import React, { Component } from 'react';
import headerImage from './header.jpg'

const ImageHeader = () => {
    const styles = {
        headerContainer: {           
            width:"100%",
        }
    };
    return (  
        
            < >
                <img src={headerImage} alt="header" style={styles.headerContainer} />
            </>
        
    );
}
 
export default ImageHeader;