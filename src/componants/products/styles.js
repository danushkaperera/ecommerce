import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(() => ({
    root: {

       position:"relative",
       marginTop:80,
  },
    media: {
        height: 260,
    },
    cardContent: {
        display: "flex",
        justifyContent: "space-between",
    },
    cartActions: {
        justifyContent: "space-between",
    },
    buttons: {
        display: "flex",
        alignItems: "center",
    },
}));
