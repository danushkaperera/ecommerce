import React, { Component } from "react";
import {
    Card,
    CardMedia,
    CardActions,
    CardContent,
    Typography,
    IconButton,
} from "@material-ui/core";
import { AddShoppingCart } from "@material-ui/icons";
import useStyles from "./styles";

const Product = ({ product, onAddToCart }) => {
    const classes = useStyles();
    return (
        <Card className={classes.root}>
            <CardMedia
                className={classes.media}
                image={product.media.source}
                title={product.name}
            />
            <CardContent>
                <div className={classes.CardContent}>
                    <Typography variant="h5" gutterBottom>
                        {product.name}
                    </Typography>

                    <Typography variant="h7">
                        {" "}
                        {product.price.formatted_with_symbol}{" "}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                        {/* {product.description} */}
                    </Typography>
                </div>
            </CardContent>
            <CardActions>
                <IconButton
                    aria-label="Add to Cart"
                    onClick={() => onAddToCart(product.id, 1)}
                >
                    <AddShoppingCart />
                </IconButton>
            </CardActions>
        </Card>
    );
};

export default Product;
