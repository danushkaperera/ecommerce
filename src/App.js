import React, { useState, useEffect } from "react";
import { Navbar, Products, ImageHeader } from "./componants";

import { commerce } from "./lib/commerce";
import "./App.css";
import { Container } from "@material-ui/core";

function App() {
    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState({});

    const fetchProducts = async () => {
        const { data } = await commerce.products.list();
        setProducts(data);
    };
    const fetchCart = async () => {
        setCart(await commerce.cart.retrieve());
    };

    const handleAddToCart = async (productId, quentity) => {
        const item = await commerce.cart.add(productId, quentity);
        setCart(item.cart);
    };
    useEffect(() => {
        fetchProducts();
        fetchCart();
    }, []);
    console.log(cart);
    return (
        <div className="App">
            <Navbar totalItem={cart.total_items} />
            <ImageHeader />
            <Container>
                <Products products={products} onAddToCart={handleAddToCart} />
            </Container>
        </div>
    );
}

export default App;
